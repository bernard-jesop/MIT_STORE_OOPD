#include "Security.hpp"
#include "Menu.hpp"

void	_printItems(WINDOW *win, int highlight, IDatabase *db, std::map<int, int> &choices, int pm);
void	_displayItems(std::map<int, int> &choices, int itemsSize, IDatabase *db);

void	_displayInvoices(std::map<int, std::string> &choices, IDatabase *db);
void	_printInvoices(WINDOW *win, int highlight, std::map<int, std::string> &choices, int *id);
void	_displayInvoice(int id, IDatabase *db);

void		displayTableWord(std::string const &value, int maxSize, char separator);
void		displayTableBorder(char value, int maxSize, char separator);
void		displayTableInt(int value, int maxSize, char separator);

void		Menu::_MS()
{
  std::map<std::string, MFP> choices;

  choices["1. Generate invoices"] = &Menu::_MSGenerateInvoice;
  choices["2. Display invoices"] = &Menu::_MSDisplayInvoice;
  choices["3. Add a new item"] = &Menu::_MSAddItem;
  choices["4. Manage item"] = &Menu::_MSManageItem;
  choices["5. Disconnect"] = &Menu::_stop;

  choices["DEBUG bills list"] = &Menu::_customerBillsList;

  while (!this->_exit)
    {
      this->displayHeader("Welcome to the MIT Store area");
      this->_displayChoices(choices);
    }
  this->_exit = false;
}

void				Menu::_MSAddItem()
{
  static std::string		item_code, name, quantity, rate, sql;

  name.clear();
  quantity.clear();
  rate.clear();
  displayHeader("Customer area - new customer form");
  mvprintw(4, 0, "Please enter the Item Code and press ENTER :\n");
  refresh();
  _getInput(item_code);
  mvprintw(7, 0, "Please enter your Name and press ENTER :\n");
  refresh();
  _getInput(name);
  mvprintw(10, 0, "Please enter the Quantity of %s you are adding and press ENTER :\n", name.c_str());
  refresh();
  _getInput(quantity);
  mvprintw(13, 0, "Please enter the Rate of %s and press ENTER :\n", name.c_str());
  refresh();
  _getInput(rate);
  item_code = Security::escape(item_code);
  name = Security::escape(name);
  quantity = Security::escape(quantity);
  rate = Security::escape(rate);
  try {
    _db->exec("BEGIN TRANSACTION");
    sql = "INSERT INTO item(code, name) VALUES(NULL,'" + name + "');";
    _db->exec(sql);
    sql = "INSERT INTO store(quantity, rate, item_code) VALUES('" + quantity + "', '" + rate + "', '" + item_code + "');";
    _db->exec(sql);
    mvprintw(16, 0, "Item successfully created.\nNew Item Code : %s\n\nPress any key to go back\n", item_code.c_str());
    _db->exec("END TRANSACTION");
  } catch (MyException const &e) {
    printw("Item was NOT created.\nError : %s.\nNew Item Code : %s\n\nPress any key to go back\n", e.what(), item_code.c_str());
    _db->exec("ROLLBACK");
  }
  getch();
}

void		Menu::_MSManageItem()
{
  IDatabase::Results			res;
  std::map<int, int>			choices;
  std::string				date;

  displayHeader("Manage item");
  mvprintw(4, 0, "Up/down to select item, +/i to select the quantity, Enter to accept.\n");

  try {
    res = _db->exec_v2("SELECT item_code, name, rate, quantity FROM store LEFT OUTER JOIN item ON item.code = store.item_code;");
    _displayItems(choices, res->size(), this->_db);
    printw("Please enter the date of the order (YYYY-MM-DD) :\n");
    _getInput(date);
    _customerGenerateBills(choices, date);
  } catch (MyException const &e) {
    mvprintw(4, 0, "Impossible to access the store!\n");
  }
}

void			Menu::_MSGenerateInvoice()
{
  IDatabase::Results	res, res2;
  std::string		sql;
  int			count = 0;

  this->displayHeader("MIT Store - invoice generation");
  try {
    res = _db->exec_v2("SELECT DISTINCT dateOrder, customer_code FROM bill WHERE NOT EXISTS ( SELECT  'x'  FROM invoice WHERE invoice.dateOrder = bill.dateOrder AND invoice.customer_code = bill.customer_code);");
    for (auto it = res->begin(); it != res->end(); ++it)
      {
	_db->exec("BEGIN TRANSACTION");
	try {
	  res2 = _db->exec_v2("SELECT item_code, quantity FROM bill WHERE dateOrder =" +  (*it)["dateOrder"] + " AND customer_code = " + (*it)["customer_code"] + ";");
	  for (auto it_item = res2->begin(); it_item != res2->end(); ++it_item)
	    std::cout << (*it_item)["quantity"] << " "  << (*it_item)["item_code"] << std::endl;
	  for (auto it_item = res2->begin(); it_item != res2->end(); ++it_item)
	    {
	      sql = "UPDATE store SET quantity = quantity - " + (*it_item)["quantity"] + " WHERE item_code = " + (*it_item)["item_code"] + ";";
	      _db->exec(sql);
	    }
	  sql = "INSERT OR IGNORE INTO invoice VALUES (NULL, '" + (*it)["dateOrder"] + "', '" + (*it)["customer_code"] + "', '0');";
	  ++count;
	  _db->exec(sql);
	  _db->exec("END TRANSACTION");
	} catch (MyException const &e) {
	  std::cerr << "error in the transaction " << e.what() << " " << e.who() << std::endl;
	  _db->exec("ROLLBACK");
	}
      }
  } catch (MyException const &e) {
    std::cerr << "Error : Generate Invoice " << e.who() << " " << e.what() << std::endl;
  }
  mvprintw(4, 0, "%d invoice(s) generated.\n\nPress any key to go back.\n", count);
  getch();
}

void				Menu::_MSDisplayInvoice()
{
  IDatabase::Results		res;
  std::string			tmp;
  std::map<int, std::string>	choices;

  this->displayHeader("Invoices");
  if (this->_connectedWithCustomer == 0)
    res = this->_db->exec_v2("SELECT id, name, dateOrder FROM invoice LEFT OUTER JOIN customer ON customer.code = invoice.customer_code;");
  else
    {
      tmp = "SELECT id, name, dateOrder FROM invoice LEFT OUTER JOIN customer ON customer.code = invoice.customer_code WHERE customer_code = '" + std::to_string(this->_connectedWithCustomer) + "';";
      res = this->_db->exec_v2(tmp.c_str());
    }
  if (res->size() == 0)
    {
      move(4, 0);
      printw("No invoices.\n\nPress any key to go back.");
      getch();
    }
  else
    {
      for (auto it = res->begin(); it != res->end(); ++it)
	{
	  tmp = (*it)["dateOrder"] + " - " + (*it)["name"];
	  choices[atoi(((*it)["id"]).c_str())] = tmp;
	}
      _displayInvoices(choices, this->_db);
    }
}

void		_displayInvoices(std::map<int, std::string> &choices, IDatabase *db)
{
  WINDOW	*win;
  int		highlight = 1, choice = 0, c = 0, id = 0;

  noecho();
  cbreak();
  win = newwin(choices.size() + 4, 80, 4, 0);
  keypad(win, true);
  refresh();
  _printInvoices(win, highlight, choices, &id);
  while (1)
    {
      c = wgetch(win);
      switch (c)
	{
	case KEY_UP:
	  if (highlight == 1)
	    highlight = choices.size();
	  else
	    --highlight;
	  break;
	case KEY_DOWN:
	  if (highlight == (int) choices.size())
	    highlight = 1;
	  else
	    ++highlight;
	  break;
	case 10:
	  choice = highlight;
	  break;
	default:
	  refresh();
	  break;
	}
      _printInvoices(win, highlight, choices, &id);
      if (choice != 0)
	break;
    }
  clrtoeol();
  refresh();
  delwin(win);
  _displayInvoice(id, db);
}

void			_printInvoices(WINDOW *win, int highlight, std::map<int, std::string> &choices, int *id)
{
  int x = 2, y = 2, i = 0;

  box(win, 0,0);
  for (std::map<int, std::string>::iterator it=choices.begin(); it != choices.end(); ++it)
    {
      if (highlight == i + 1)
	{
	  *id = it->first;
	  wattron(win, A_REVERSE);
	  mvwprintw(win, y, x, "%s", it->second.c_str());
	  wattroff(win, A_REVERSE);
	}
      else
	{
	  mvwprintw(win, y, x, "%s", it->second.c_str());
	}
      ++y;
      ++i;
    }
  wrefresh(win);
  move(choices.size() + 3, 0);
  refresh();
}

void			_displayInvoice(int id, IDatabase *db)
{
  IDatabase::Results	res, res2;
  std::string		sql;
  int			value = 0, total = 0;

  clear();
  move(0, 0);
  sql = "SELECT name, dateOrder, customer_code FROM invoice LEFT OUTER JOIN customer ON customer.code = invoice.customer_code WHERE id = '" + std::to_string(id) + "';";
  res = db->exec_v2(sql.c_str());
  for (auto it = res->begin(); it != res->end(); ++it)
    {
      printw("MIT Appliances\nAB - 9, MIT Building, Manipal Institute of Technology, Manipal\nInvoice\n\n");
      printw("Date: %s\nInvoice number: %d\nCustomer: %s\n\n", ((*it)["dateOrder"]).c_str(), id, ((*it)["customer_code"]).c_str());
      printw("Dear %s,\nGiven below are the details of the items you had purchased from us in the last month.\n\n", ((*it)["name"]).c_str());
      sql = "SELECT bill.item_code, item.name, store.rate, bill.quantity FROM bill LEFT OUTER JOIN item ON item.code = bill.item_code LEFT OUTER JOIN store ON store.item_code = bill.item_code WHERE bill.dateOrder = '" + (*it)["dateOrder"] + "' AND bill.customer_code = '" + (*it)["customer_code"] + "';";
      res2 = db->exec_v2(sql.c_str());
      displayTableBorder('-', 4, '+');
      displayTableBorder('-', 30, '+');
      displayTableBorder('-', 4, '+');
      displayTableBorder('-', 7, '+');
      displayTableBorder('-', 10, '+');
      printw("+\n");
      displayTableWord("Code", 4, '|');
      displayTableWord("Name", 30, '|');
      displayTableWord("Qty", 4, '|');
      displayTableWord("Price", 7, '|');
      displayTableWord("Value", 10, '|');
      printw("|\n");
      displayTableBorder('-', 4, '+');
      displayTableBorder('-', 30, '+');
      displayTableBorder('-', 4, '+');
      displayTableBorder('-', 7, '+');
      displayTableBorder('-', 10, '+');
      printw("+\n");
      for (auto it2 = res2->begin(); it2 != res2->end(); ++it2)
	{
	  value = atoi(((*it2)["rate"]).c_str()) * atoi(((*it2)["quantity"]).c_str());
	  total += value;
	  displayTableWord(((*it2)["item_code"]), 4, '|');
	  displayTableWord(((*it2)["name"]), 30, '|');
	  displayTableWord(((*it2)["quantity"]), 4, '|');
	  displayTableWord(((*it2)["rate"]), 7, '|');
	  displayTableWord(std::to_string(value), 10, '|');
	  printw("|\n");
	}
      displayTableBorder('-', 4, '+');
      displayTableBorder('-', 30, '+');
      displayTableBorder('-', 4, '+');
      displayTableBorder('-', 7, '+');
      displayTableBorder('-', 10, '+');
      printw("+\n\n");
      printw("Total value: %d\n\n", total);
      printw("In case any discrepancy; please bring it to our notice within 7 days of the invoice date,\nfailing which the details would be considered as correct.\n\nThe payment should reach us within 10 days of the invoice date.\n\nPrepared by: ____________________\n\nChecked by:  ____________________\n");
      break;
    }
  getch();
}

void		displayTableWord(std::string const &value, int maxSize, char separator)
{
  int		t, r, f, s, i;

  printw("%c", separator);
  t = (maxSize + 2) - value.size();
  r = t / 2;
  if (r * 2 == t)
    {
      f = r;
      s = r;
    }
  else
    {
      f = r + 1;
      s = r;
    }
  for (i = 0; i < f; ++i)
    printw("%c", ' ');
  printw("%s", value.c_str());
  for (i = 0; i < s; ++i)
    printw("%c", ' ');
}

void		displayTableBorder(char value, int maxSize, char separator)
{
  int		i;

  printw("%c", separator);
  for (i = 0; i < maxSize + 2; ++i)
    printw("%c", value);
}

void		displayTableInt(int value, int maxSize, char separator)
{
  //int		i;

  maxSize = maxSize;
  printw("%c ", separator);
  if (value < 10)
    printw("%c", '0');
  if (value < 100)
    printw("%c", '0');
  if (value < 1000)
    printw("%c", '0');
  printw("%d", value);
  printw("%c", ' ');
 }

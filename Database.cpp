#include <cassert>
#include "Database.hh"
#include "MyException.hh"

Database::Database(std::string const &database_name)
{
  int	ret;

  ret = sqlite3_open(database_name.c_str(),  &_db);
  if (SQLITE_OK != ret)
    {
      throw MyException("Openning databse have failed " + ret);
    }
}

Database::Database() : Database("") {}

Database::~Database()
{
  int	ret;

  ret = sqlite3_close(_db);
  if (SQLITE_OK != ret)
    {
      throw MyException("Openning databse have failed" + ret);
    }
}

int		Database::exec(char const * query)
{
  int		ret;
  sqlite3_stmt	*ppStmt;

  assert(query != NULL);
  if (SQLITE_OK != sqlite3_prepare_v2(_db, query, -1, &ppStmt, NULL))
    throw MyException(std::string("Failed : ") + sqlite3_errmsg(_db), query);
  while (SQLITE_ROW == (ret = sqlite3_step(ppStmt)))
    ;
  if (SQLITE_DONE != ret)
    throw MyException("Something wrong happend with the statement " + ret);
  sqlite3_finalize(ppStmt);
  return (0);
}

IDatabase::Results								Database::exec_v2(char const * query)
{
  int										ret;
  sqlite3_stmt									*ppStmt;
  std::unique_ptr<std::vector<std::map<std::string const, std::string> > >	res(new std::vector<std::map<std::string const, std::string> >);

  assert(query != NULL);
  if (SQLITE_OK != sqlite3_prepare_v2(_db, query, -1, &ppStmt, NULL))
    throw MyException("Failed to create SQL query", query);
  while (SQLITE_ROW == (ret = sqlite3_step(ppStmt)))
    {
      std::map<std::string const, std::string>	tmp;
      int	nb_col = sqlite3_column_count(ppStmt);

      for (int i = 0; i < nb_col; ++i)
	{
	  tmp[std::string(sqlite3_column_name(ppStmt, i))] = std::string(reinterpret_cast<const char *>(sqlite3_column_text(ppStmt, i)));
	}
      res->push_back(tmp);
    }
  if (SQLITE_DONE != ret)
    throw MyException(sqlite3_errmsg(_db) + ret);
  sqlite3_finalize(ppStmt);
  return (res);
}

IDatabase::Results	Database::exec_v2(std::string const &query)
{
  return (exec_v2(query.c_str()));
}

int		Database::exec(std::string const &query)
{
  return (exec(query.c_str()));
}

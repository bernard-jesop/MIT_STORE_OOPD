#ifndef IDATABASE_HH_
# define IDATABASE_HH_

#include <string>
#include <vector>
#include <map>
#include <memory>

class	IDatabase
{
public:
  using Results =  std::unique_ptr<std::vector<std::map<std::string const, std::string> > >;
  virtual ~IDatabase() {}

  virtual int		exec(std::string const &query) = 0;
  virtual int		exec(char const * query) = 0;
  virtual Results	exec_v2(char const * query) = 0;
  virtual Results	exec_v2(std::string const &query) = 0;
};

#endif	// IDATABASE_HH_

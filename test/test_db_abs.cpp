#include "Database.hh"

int		test_db_abs()
{
  Database	toto("test1.db");

  try
    {
      toto.exec("CREATE TABLE IF NOT EXISTS demo (name TEXT, age INTEGER);");
      toto.exec("insert into demo (name, age) values ('Tom', 20);");
      toto.exec("select * from demo;");
    }
  catch (MyException const &e)
    {
      std::cerr << e.what() << " " << e.who() << std::endl;
    }
  return (0);
}

int		test_db_abs_v2()
{
  Database	toto("test1.db");

  try
    {
      std::unique_ptr<std::vector<std::map<std::string const, std::string> > >	res;
      res = toto.exec_v2("CREATE TABLE IF NOT EXISTS demo (name TEXT, age INTEGER);");
      res = toto.exec_v2("insert into demo (name, age) values ('Tom', 20);");
      res = toto.exec_v2("select * from demo;");
      // for (auto it = res->begin(); it != res->end(); ++it)
      // 	{
      // 	  std::cout << "0name " << (*it)["name"] << std::endl;
      // 	  std::cout << "0age " << (*it)["age"] << std::endl;
      // 	}
    }
  catch (MyException const &e)
    {
      std::cerr << e.what() << " " << e.who() << std::endl;
    }
  return (0);
}

int		test_db_interface_v2()
{
  std::unique_ptr<IDatabase>	db(new Database("test.db"));

  try
    {
      IDatabase::Results	res;
      res = db->exec_v2("CREATE TABLE IF NOT EXISTS demo (name TEXT, age INTEGER);");
      res = db->exec_v2("insert into demo (name, age) values ('Tom', 20);");
      res = db->exec_v2("select * from demo;");
      for (auto it = res->begin(); it != res->end(); ++it)
      	{
      	  std::cout << "0name " << (*it)["name"] << std::endl;
      	  std::cout << "0age " << (*it)["age"] << std::endl;
      	}
    }
  catch (MyException const &e)
    {
      std::cerr << e.what() << " " << e.who() << std::endl;
    }
  return (0);
}

int	main()
{
  // test_db_abs();
  // test_db_interface_v2();
  test_db_interface_v2();
  return (0);
}

#ifndef		SECURITY_HPP_
# define		SECURITY_HPP_

# include <string>
# include <algorithm>
# include <openssl/sha.h>
# include <cstdio>

namespace	Security
{
  std::string		escape(std::string str);
  std::string		sha1(std::string str);
}

#endif

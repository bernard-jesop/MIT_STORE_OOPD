CC		=	gcc

CXX		=	g++

RM		=	rm -f

CXXFLAGS	+=	-g3 -std=c++11 -W -Wall -Wextra -Werror \
			-I ./

LDFLAGS		+=	-lpthread -ldl -lncurses -lcrypto

SRCS_COMMON	=	Menu.cpp \
			MenuCustomer.cpp \
			MenuMS.cpp \
			MyException.cpp \
			Database.cpp \
			Security.cpp

SRCS		=	main.cpp \
			$(SRCS_COMMON)

OBJS		=	$(SRCS:.cpp=.o)

SRCSSQL		=	sqlite3.c

OBJSSQL		=	$(SRCSSQL:.c=.o)

NAME		=	mit_store

SRCS_TEST	=	./test/test_db_abs.cpp \
			$(SRCS_COMMON)

OBJS_TEST	=	$(SRCS_TEST:.cpp=.o)

all: $(NAME)

$(NAME): $(OBJSSQL) $(OBJS)
	$(CXX) $(CXXFLAGS) $(OBJS) $(OBJSSQL) $(LDFLAGS) -o $(NAME)

clean:
	$(RM) $(OBJS) $(OBJSSQL)

fclean: clean
	$(RM) $(NAME)

test: $(OBJSSQL) $(OBJS_TEST)
	$(CXX) $(CXXFLAGS) $(OBJS_TEST) $(OBJSSQL) $(LDFLAGS) -o test_$(NAME)

re: fclean all

.PHONY: all clean fclean re

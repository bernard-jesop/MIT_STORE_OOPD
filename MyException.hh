#ifndef MYEXCEPTION_HH_
# define MYEXCEPTION_HH_

#include <stdexcept>

class MyException : public std::exception
{
  std::string	_who;
  std::string	_what;

public:
  MyException(std::string const &_what, std::string const &_who = "");
  MyException();
  virtual ~MyException() throw ();
  MyException(MyException const &other);
  MyException const &operator=(MyException const &other);

  virtual const char *what() const throw();
  virtual const char *who() const throw();
};

#endif	// MYEXCEPTION_HH_

#include "Security.hpp"
#include "Menu.hpp"

void		_printItems(WINDOW *win, int highlight, IDatabase *db, std::map<int, int> &choices, int pm);
void		_displayItems(std::map<int, int> &choices, int itemsSize, IDatabase *db);

void			Menu::_customerLogin()
{
  IDatabase::Results	res;
  std::string		code, pwd, sql;

  displayHeader("Customer area - login form");
  mvprintw(4, 0, "Please enter your Code and press ENTER :\n");
  refresh();
  _getInput(code);
  mvprintw(7, 0, "Please enter your Password and press ENTER :\n");
  refresh();
  _getInput(pwd, true);
  pwd = Security::sha1(pwd);
  code = Security::escape(code);
  sql = "SELECT code, name FROM customer WHERE code = '" + code + "' AND password = '" + pwd + "' LIMIT 1;";
  try {
    res = this->_db->exec_v2(sql.c_str());
    if (res->size() == 0)
      {
	mvprintw(10, 0, "No account found with these credentials. Press any key to go back.\n");
	getch();
      }
    else
      {
	this->_connectedWithCustomer = atoi(code.c_str());
	this->_customerMenu();
      }
  } catch (MyException const &e) {
    mvprintw(10, 0, "No account found with these credentials. Press any key to go back.\n");
    getch();
  }
}

void			Menu::_customerAdd()
{
  bool			noError = false;
  std::string		name, address, pwd, pwd2, sql, id;
  IDatabase::Results	res;

  while (noError == false)
    {
      name.clear();
      address.clear();
      pwd.clear();
      pwd2.clear();
      sql.clear();
      this->displayHeader("Customer area - new customer form");
      mvprintw(4, 0, "Please enter your Name and press ENTER :\n");
      refresh();
      this->_getInput(name);
      move(7, 0);
      printw("Please enter your Address and press ENTER :\n");
      refresh();
      this->_getInput(address);
      move(10, 0);
      printw("Please enter your Password and press ENTER :\n");
      refresh();
      this->_getInput(pwd, true);
      move(13, 0);
      printw("Please confirm your Password and press ENTER :\n");
      refresh();
      this->_getInput(pwd2, true);
      if (pwd == pwd2)
	{
	  pwd = Security::sha1(pwd);
	  name = Security::escape(name);
	  address = Security::escape(address);
	  sql = "INSERT INTO customer(code, name, address, password) VALUES(NULL, '" + name + "', '" + address + "', '" + pwd + "');";
	  _db->exec(sql);
	  mvprintw(16, 0, "Account successfully created.\nNew Customer Code : %s\n\nPress any key to go back\n", id.c_str());
	  noError = true;
	}
      else
	{
	  mvprintw(16, 0, "The passwords are not the same.\nPress any key to retry.\n");
	  getch();
	}
    }
  getch();
}

void			Menu::_customerList()
{
  IDatabase::Results	res;

  this->displayHeader("Customer area - customer list");
  move(4, 0);
  try {
    res = this->_db->exec_v2("SELECT * FROM customer ORDER BY code ASC;");
    if (res->size() == 0)
      printw("No elements");
    else
      {
	printw("code|name|address|password\n");
	for (auto it = res->begin(); it != res->end(); ++it)
	  {
	    printw("%s|%s|%s|%s\n",
		   ((*it)["code"]).c_str(),
		   ((*it)["name"]).c_str(),
		   ((*it)["address"]).c_str(),
		   ((*it)["password"]).c_str()
		   );
	  }
      }
  } catch (MyException const &e) {
    std::cerr << e.what() << " " << e.who() << std::endl;
  }
  getch();
}

void		Menu::_customerMenu()
{
  std::map<std::string, MFP> choices;

  choices["1. Buy item from MIT Store"] = &Menu::_customerBuyItemFromStore;
  choices["2. Display invoices"] = &Menu::_MSDisplayInvoice;
  choices["3. Disconnect"] = &Menu::_stop;
  choices["DEBUG bills list"] = &Menu::_customerBillsList;

  while (!_exit)
    {
      displayHeader("Welcome customer " + std::to_string(this->_connectedWithCustomer));      
      _displayChoices(choices);
    }
  _exit = false;
  _connectedWithCustomer = 0;
}

void					Menu::_customerBuyItemFromStore()
{
  IDatabase::Results			res;
  std::map<int, int>			choices;
  std::string				date;

  displayHeader("Buy item");
  mvprintw(4, 0, "Up/down to select item, +/i to select the quantity, Enter to accept.\n");

  try {
    res = _db->exec_v2("SELECT item_code, name, rate, quantity FROM store LEFT OUTER JOIN item ON item.code = store.item_code;");
    _displayItems(choices, res->size(), this->_db);
    printw("Please enter the date of the order (YYYY-MM-DD) :\n");
    _getInput(date);
    _customerGenerateBills(choices, date);
  } catch (MyException const &e) {
    mvprintw(4, 0, "Impossible to access the store!\n");
  }
}

void		_displayItems(std::map<int, int> &choices, int itemsSize, IDatabase *db)
{
  WINDOW	*win;
  int		highlight = 1, choice = 0, c = 0, pm = 0;

  noecho();
  cbreak();
  win = newwin(itemsSize + 4, 80, 6, 0);
  keypad(win, true);
  refresh();
  _printItems(win, highlight, db, choices, pm);
  while (1)
    {
      pm = 0;
      c = wgetch(win);
      switch(c)
	{
	case KEY_UP:
	  if (highlight == 1)
	    highlight = itemsSize;
	  else
	    --highlight;
	  break;
	case KEY_DOWN:
	  if (highlight == itemsSize)
	    highlight = 1;
	  else
	    ++highlight;
	  break;
	case 10:
	  choice = highlight;
	  break;
	case 43:
	case 112:
	  pm = 1;
	  break;
	case 45:
	case 109:
	  pm = -1;
	  break;
	default:
	  refresh();
	  break;
	}
      _printItems(win, highlight, db, choices, pm);
      if (choice != 0)
	break;
    }
  clrtoeol();
  refresh();
  delwin(win);
}

void			_printItems(WINDOW *win, int highlight, IDatabase *db, std::map<int, int> &choices, int pm)
{
  IDatabase::Results	res;
  int			x = 2, y = 2, i = 0, id = 0, qty = 0, rate = 0, total = 0, max_qty = 0;

  res = db->exec_v2("SELECT item_code, name, rate, quantity FROM store LEFT OUTER JOIN item ON item.code = store.item_code;");
  box(win, 0,0);
  for (auto it = res->begin(); it != res->end(); ++it)
    {
      id = atoi(((*it)["item_code"]).c_str());
      max_qty = atoi(((*it)["quantity"]).c_str());
      rate = atoi(((*it)["rate"]).c_str());
      if (highlight == i + 1)
	{
	  choices[id] += pm;
	}
      qty = choices[id] ? choices[id] : 0;
      if (qty < 0)
	{
	  qty = 0;
	  choices[id] = 0;
	}
      if (qty > max_qty)
	{
	  qty = max_qty;
	  choices[id] = max_qty;
	}
      total += (qty * rate);
      mvwprintw(win, y, x, "                                                                     ");
      if (highlight == i + 1)
	{
	  wattron(win, A_REVERSE);
	  mvwprintw(win, y, x, "%d/%s (%d Rps) - %s (%s Rps)", qty, ((*it)["quantity"]).c_str(), (qty * rate), ((*it)["name"]).c_str(), ((*it)["rate"]).c_str());
	  wattroff(win, A_REVERSE);
	}
      else
	{
	  mvwprintw(win, y, x, "%d/%s (%d Rps) - %s (%s Rps)", qty, ((*it)["quantity"]).c_str(), (qty * rate), ((*it)["name"]).c_str(), ((*it)["rate"]).c_str());
	}
      ++y;
      ++i;
    }
  wrefresh(win);
  move(res->size() + 3 + 4 + 3, 0);
  printw("                                                       ");
  move(res->size() + 3 + 4 + 3, 0);
  printw("Total : %d Rps\n\n", total);
  refresh();
}

void			Menu::_customerGenerateBills(std::map<int, int> &choices, std::string &date)
{
  int			total = 0;
  std::string		sql;

  printw("\n\n");
  for (std::map<int, int>::iterator it = choices.begin() ; it != choices.end(); ++it)
    total += it->second;
  if (total == 0)
    {
      printw("No item bought, transaction cancelled.\n\nPress any key to go back.\n");
      getch();
      return;
    }
  for (std::map<int, int>::iterator it = choices.begin() ; it != choices.end(); ++it)
    {
      if (it->second != 0)
	{
	  sql = "INSERT INTO bill VALUES ('" + Security::escape(date) + "', '" + std::to_string(this->_connectedWithCustomer) + "', '" + std::to_string(it->first) + "', '" + std::to_string(it->second) + "');";
	  this->_db->exec_v2(sql.c_str());
	}
    }
  printw("Transaction saved ! Waiting for the reply of MIT Store.\n\nPress any key to go back.\n");
  getch();
}

void			Menu::_customerBillsList()
{
  IDatabase::Results	res;

  this->displayHeader("Customer area - customer bill list");
  move(4, 0);
  res = this->_db->exec_v2("SELECT * FROM bill ORDER BY dateOrder ASC;");
  if (res->size() == 0)
    printw("No elements");
  else
    {
      printw("dateOrder|customer_code|item_code|quantity\n");
      for (auto it = res->begin(); it != res->end(); ++it)
	{
	  printw("%s|%s|%s|%s\n",
		 ((*it)["dateOrder"]).c_str(),
		 ((*it)["customer_code"]).c_str(),
		 ((*it)["item_code"]).c_str(),
		 ((*it)["quantity"]).c_str()
		 );
	}
    }
  getch();
}

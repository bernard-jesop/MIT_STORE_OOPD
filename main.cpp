#include "Menu.hpp"
#include <iostream>
#include "sqlite3.h"

int		main()
{
  Menu		menu;

  initscr();
  curs_set(0);
  keypad(stdscr, true);
  menu.login();
  getch();
  curs_set(1);
  endwin();
  return (0);
}

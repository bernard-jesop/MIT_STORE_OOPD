#include "Security.hpp"

std::string		Security::escape(std::string str)
{
  size_t		start_pos = 0;
  std::string	from = "'", to = "\\'";

  while ((start_pos = str.find(from, start_pos)) != std::string::npos)
    {
      str.replace(start_pos, from.length(), to);
      start_pos += to.length();
    }
  return (str);
}

std::string		Security::sha1(std::string str)
{
  unsigned char	hash[SHA_DIGEST_LENGTH];
  char		*temp = new char[5];

  SHA1((unsigned char *)str.c_str(), str.size(), hash);
  str.clear();
  for (int i = 0 ; i < SHA_DIGEST_LENGTH; ++i)
    {
      sprintf(temp, "%02x", hash[i]);
      str += temp;
    }
  return (str);
}

#include "MyException.hh"

MyException::MyException(std::string const &what, std::string const &who)
{
  _who = who;
  _what = what;
}

MyException::MyException() : MyException("", "") {}
MyException::~MyException() throw() {}
MyException::MyException(MyException const &other) : MyException(other._what, other._who) {}
MyException const &MyException::operator=(MyException const &other)
{
  if (this != &other)
    {
      _who = other._who;
      _what = other._what;
    }
  return (*this);
}

const char *MyException::what() const throw()
{
  return (_what.c_str());
}

const char *MyException::who() const throw()
{
  return (_who.c_str());
}

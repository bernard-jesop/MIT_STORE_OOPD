#ifndef DATABASE_HH_
# define DATABASE_HH_

#include <iostream>
#include "IDatabase.hpp"
#include "sqlite3.h"
#include "MyException.hh"

class		Database : public IDatabase
{
  sqlite3	*_db;

public:
  explicit Database(std::string const &database_name);
  Database();
  ~Database();
  Database(Database const &other);
  Database const &operator=(Database const &other);

  virtual int			exec(std::string const &query) final;
  virtual int			exec(char const * query) final;
  virtual IDatabase::Results	exec_v2(char const * query) final;
  virtual IDatabase::Results	exec_v2(std::string const &query) final;
};

#endif	// DATABASE_HH_

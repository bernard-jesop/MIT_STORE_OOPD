#include "Menu.hpp"

Menu::Menu()
{
  this->_exit = false;
  this->_db = new Database("MS.db");
  this->_initDb();
}

Menu::~Menu()
{
  delete this->_db;
}

void		Menu::_initDb()
{
  try {
    _db->exec_v2("PRAGMA foreign_keys = ON;");
    _db->exec_v2("CREATE TABLE IF NOT EXISTS customer ( \
code INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, \
name VARCHAR(65) NOT NULL, \
address VARCHAR(65) NOT NULL, \
password VARCHAR(65) NOT NULL);");
    _db->exec_v2("CREATE TABLE IF NOT EXISTS item ( \
code INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, \
name VARCHAR(65) NULL);");
    _db->exec_v2("CREATE TABLE IF NOT EXISTS store ( \
quantity INT NULL CHECK(quantity >= 0), \
rate REAL NULL CHECK(rate > 0), \
item_code INTEGER PRIMARY KEY NOT NULL, \
FOREIGN KEY(item_code) REFERENCES item(code));");
    _db->exec_v2("CREATE TABLE IF NOT EXISTS bill ( \
dateOrder DATE NOT NULL CHECK(JULIANDAY(dateOrder) IS NOT NULL), \
customer_code INTEGER NOT NULL, \
item_code INTEGER NOT NULL, \
quantity INT NULL CHECK(quantity >= 0), \
FOREIGN KEY(customer_code) REFERENCES customer(code), \
FOREIGN KEY (item_code) REFERENCES item(code));");
    _db->exec_v2("CREATE TABLE IF NOT EXISTS invoice ( \
id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, \
dateOrder DATE NOT NULL CHECK(JULIANDAY(dateOrder) IS NOT NULL), \
customer_code  INT NOT NULL, \
total_price INT NOT NULL, \
FOREIGN KEY (customer_code) REFERENCES customer(code) \
UNIQUE (dateOrder, customer_code));");
  } catch (MyException const &e) {
    std::cerr << "Database intialisation have failed"  << std::endl;
    std::cerr << e.what() << " by " << e.who()  << std::endl;
    std::terminate();
  }
}

void		Menu::displayHeader(const std::string &title)
{
  int		f, t, r;
  WINDOW	*local_win;

  clear();
  refresh();
  local_win = newwin(3, 80, 0, 0);
  wborder(local_win, '#', '#', '#', '#', '#', '#', '#', '#');
  t = (80 - 2) - title.size();
  r = t / 2;
  f = (r * 2 == t) ? r : r + 1;
  mvprintw(1, f, "%s", title.c_str());
  move(3,0);
  wrefresh(local_win);
  refresh();
  delwin(local_win);
}

void		Menu::_notImplemented()
{
  this->displayHeader("Not implemented");
  move(4, 0);
  printw("Press any key to go back");
  getch();
}

void		Menu::_mitStore()
{
  this->_MS();
}

void		Menu::_customer()
{
  std::map<std::string, MFP> choices;

  choices["1. Existing customer"] = &Menu::_customerLogin;
  choices["2. New customer"] = &Menu::_customerAdd;
  choices["3. Back"] = &Menu::_stop;
  choices["DEBUG list"] = &Menu::_customerList;

  while (!this->_exit)
    {
      this->displayHeader("Customer area");
      this->_displayChoices(choices);
    }
  this->_exit = false;
}

void		Menu::_supplier()
{
  this->_notImplemented();
}

void		Menu::_stop()
{
  this->_exit = true;
}

void		Menu::_bye()
{
  this->displayHeader("Bye !");
  move(4, 0);
  printw("Press any key to exit");
}

void		Menu::login()
{
  std::map<std::string, MFP> choices;

  choices["1. MIT Store"] = &Menu::_mitStore;
  choices["2. A Customer"] = &Menu::_customer;
  choices["3. A Supplier"] = &Menu::_supplier;
  choices["4. Exit"] = &Menu::_stop;

  while (!this->_exit)
    {
      this->displayHeader("Who are you ?");
      this->_displayChoices(choices);
    }
  this->_bye();
}

void		Menu::_displayChoices(std::map<std::string, MFP> &choices)
{
  WINDOW	*win;
  int		highlight = 1, choice = 0, c = 0;

  noecho();
  cbreak();
  win = newwin(choices.size() + 4, 80, 3, 0);
  keypad(win, true);
  refresh();
  this->_printChoices(win, highlight, choices);
  while(1)
    {
      c = wgetch(win);
      switch(c)
	{
	case KEY_UP:
	  if (highlight == 1)
	    highlight = choices.size();
	  else
	    --highlight;
	  break;
	case KEY_DOWN:
	  if (highlight == (int) choices.size())
	    highlight = 1;
	  else
	    ++highlight;
	  break;
	case 10:
	  choice = highlight;
	  break;
	default:
	  refresh();
	  break;
	}
      this->_printChoices(win, highlight, choices);
      if (choice != 0)
	break;
    }
  clrtoeol();
  refresh();
  delwin(win);
  this->_callbackMenu(choices, choice - 1);
}

void		Menu::_printChoices(WINDOW *win, int highlight, std::map<std::string, MFP> &choices)
{
  int x = 2, y = 2, i = 0;

  box(win, 0,0);
  for (std::map<std::string, MFP>::iterator it=choices.begin(); it != choices.end(); ++it)
    {
      if (highlight == i + 1)
	{
	  wattron(win, A_REVERSE);
	  mvwprintw(win, y, x, "%s", it->first.c_str());
	  wattroff(win, A_REVERSE);
	}
      else
	{
	  mvwprintw(win, y, x, "%s", it->first.c_str());
	}
      ++y;
      ++i;
    }
  wrefresh(win);
  move(choices.size() + 3 + 4, 0);
  refresh();
}

void		Menu::_callbackMenu(std::map<std::string, MFP> &choices, int choice)
{
  int		i = 0;

  for (std::map<std::string, MFP>::iterator it=choices.begin(); it != choices.end(); ++it)
    {
      if (i == choice)
	(this->*choices[it->first])();
      ++i;
    }
}

void		Menu::_getInput(std::string &value, bool hideInput)
{
  int		c = 0, size = 0;

  value.clear();
  curs_set(1);
  while (1)
    {
      c = getch();
      if (c > 31 && c < 137)
	{
	  if (size < 65)
	    {
	      if (hideInput)
		printw("*");
	      else
		printw("%c", c);
	      value += c;
	      ++size;
	    }
	}
      if (c == 263)
	{
	  if (size > 0)
	    {
	      printw("\b \b");
	      value = value.substr(0, size - 1);
	      --size;
	    }
	}
      if (c == 10 && size > 0)
	break;
    }
  curs_set(0);
}

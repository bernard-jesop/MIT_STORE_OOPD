#ifndef		MENU_HPP_
#define		MENU_HPP_

#include <string>
#include <map>
#include <ncurses.h>
#include "Database.hh"

class		Menu
{
private:
  IDatabase	*_db;
  bool		_exit;
  int		_connectedWithCustomer;
  int		_connectedWithSupplier;
  std::string	_connectedName;
public:
  typedef void	(Menu::*MFP)();
private:
  void		_initDb();
  void		_mitStore();
  void		_customer();
  void		_supplier();
  void		_stop();
  void		_bye();
  void		_displayChoices(std::map<std::string, MFP> &);
  void		_printChoices(WINDOW *, int, std::map<std::string, MFP> &);
  void		_callbackMenu(std::map<std::string, MFP> &, int);
  void		_getInput(std::string &, bool = false);
  void		_notImplemented();
public:
  Menu();
  ~Menu();
  void		displayHeader(const std::string &);
  void		login();
private:
  void		_customerLogin();
  void		_customerAdd();
  void		_customerList();
  void		_customerBillsList();
  void		_customerMenu();
  void		_customerBuyItemFromStore();
  void		_customerGenerateBills(std::map<int, int> &, std::string &);
private:
  void		_MS();
  void		_MSGenerateInvoice();
  void		_MSDisplayInvoice();
  void		_MSAddItem();
  void		_MSManageItem();
};

#endif
